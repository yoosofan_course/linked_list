This is a simple step by step teaching linked list in C++ by `Ahmad Yoosofan <yoosofan.github.io>`_

2018/12/13 22:24:16

Command line instructions
####################################################################################################
.. code:: sh

	Git global setup
	git config --global user.name "Ahmad Yoosofan"
	git config --global user.email "yoosofan@fastmail.fm"

Create a new repository
####################################################################################################
.. code:: sh

	git clone git@gitlab.com:yoosofan_course/linked_list.git
	cd linked_list
	touch README.md
	git add README.md
	git commit -m "add README"
	git push -u origin master

Existing folder
####################################################################################################
.. code:: sh

	cd existing_folder
	git init
	git remote add origin git@gitlab.com:yoosofan_course/linked_list.git
	git add .
	git commit -m "Initial commit"
	git push -u origin master

Existing Git repository
####################################################################################################
.. code:: sh

	cd existing_repo
	git remote rename origin old-origin
	git remote add origin git@gitlab.com:yoosofan_course/linked_list.git
	git push -u origin --all
	git push -u origin --tags

https://gitlab.com/yoosofan_course/linked_list

.. code:: sh

	git remote add origin git@gitlab.com:yoosofan_course/linked_list.git

	git remote add origin https://gitlab.com/yoosofan_course/linked_list.git

https://gitlab.com/yoosofan_course/linked_list.git
