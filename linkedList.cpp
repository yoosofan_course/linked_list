/*
 * Ahmad Yoosofan
 * http://yoosofan.github.io
 * yoosofan@mail.com
 * University of Kashan
 * Copy righted
 */
#include <iostream>
using namespace std;
template<typename T> class myList;
template <typename T>
class Node{
	T data;
	Node<T>* next;
	public:
	Node(T d=0){data=d;next=nullptr;}
	friend class myList<T>; 
	template<typename KV> friend ostream&operator<<(ostream&,const myList<KV>&);
};
template<typename T>
class myList{
	Node<T> head;
	public:
	myList(){head.next=nullptr;}
	void insert_first(T d){
		Node<T>*p=head.next;
		head.next=new Node<T>(d);
		head.next->next=p;
	}
	void append(T d=0){
		Node<T>*p=&head;
		for(;p->next!=nullptr;p=p->next);
		p->next=new Node<T>(d);
	}
	T& operator[](const int index){
		Node<T>*p=head.next;int i;
		for(i=0;i<index && p->next!=nullptr;i++,p=p->next)
			;
		if(i==index) return p->data;
		throw 2;
	}
	void remove(int index){
		Node<T>*p=&head;int i=0;
		for(;i<index && p->next!=nullptr;i++,p=p->next);
		if(i==index){
			Node<T>*p2=p->next->next;
			delete p->next;
			p->next=p2;
		}
	}
	void print(void) const{
		Node<T>*p=head.next;
		for(;p!=nullptr;p=p->next)
			cout<<p->data<<endl;
	}
	~myList(){Node<T>*p;cout<<"in destructor"<<endl;
		for(Node<T>*p2=head.next;p2!=nullptr;){
		    p=p2->next;
		    delete p2;
		    p2=p;
		}
		head.next=nullptr;
	}
	template<typename KV> friend ostream&operator<<(ostream&,const myList<KV>&);
	void copy(const myList<T>&a){
		Node<T>* p1=a.head.next, *p2=&this->head;
		this->head.next=nullptr;
		for(;p1!=nullptr;p1=p1->next){
			p2->next = new Node<T>(p1->data);
			p2=p2->next;
		}
	}
	myList<T>& operator=(const myList<T>& a){
		this->~myList();head.next=nullptr;
		copy(a);
		return *this;
	}
	myList(const myList<T>&a){copy(a);}
};

template<typename T>
ostream& operator<<(ostream &o1,const myList<T> & a1){
		Node<T>*p=a1.head.next;
		for(;p!=nullptr;p=p->next)
			o1<<p->data<<endl;
}	
template<typename T>
void f1(myList<T>&l1){
	l1.insert_first(3);
	l1.insert_first(4);
	l1.insert_first(5);
	cout<<"l1[0]=\t"<<l1[0]<<endl;
	cout<<"l1[2]=\t"<<l1[2]<<endl;
	l1[1]=456;
	cout<<"l1[1]=\t"<<l1[1]<<endl;
	cout<<l1;//l1.print();
	myList<double> l2=l1,l3,l4;
	l4=l3=l2;
	l1.remove(1);cout<<"l1 after remove"<<endl<<l1;
	cout<<"l2:"<<endl<<l2;
}
int main(){
	myList<double> l1;//l1.head.next=nullptr;
	f1<double>(l1);
	cout<<"in main"<<endl<<l1;
	return 0;
}
